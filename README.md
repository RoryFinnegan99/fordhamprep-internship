# Internship Project

For my internship at Fordham Preparatory School I have undertaken the task of 
creating a chatserver and instructing/tutoring this year's Computer Science
Advanced Honors class. 

## Chat Server

The centerpiece of my internship revolves around the creation and maintanence
of a Chat Server application created in Java. This application allows users to
register with unique usernames and have their passwords stored safely in a
hash. This application will manage user to user interaction. Alongside the
chatserver a handful of smaller methods are available to users, allowing them
to tinker with and receive server information.

### Goals

- [x] Multiple user's connected at once
- [x] SQLite Support
- [x] PostgreSQL Support
- [x] Command Support
- [ ] MySQL Support
- [ ] Permissions System

### Dependencies

* JDK 7

* JBCrypt

* SQLite JDBC

* PostgreSQL JDBC

* MySQL JDBC(unimplemented)

## Java Client

This client is written in Java as to enable support for all platforms and
architectures that have the runtime ported for their native use. Although
there is a GUI in development, the final product and newest features are
designed only for compatability with the CLI program. The project stresses 
importance for compatability on headless devices.

### Goals

- [x] Working CLI application
- [x] User specified server
- [ ] Functional GUI application 

### Dependencies

* JRE 7

* JBCrypt
