//MIT License
//
//Copyright (c) 2017 Rory Finnegan
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

package org.fordhamprep.finneganr.chat.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.fordhamprep.finneganr.chat.server.managers.LoginManager;
import org.fordhamprep.finneganr.chat.server.tools.Command;

public class SessionHandler extends Thread {
	/*
	 * Contains a user's username in order to be passed to a class used for
	 * authenticating user information
	 */
	public String userName;

	/*
	 * Contains a hash of the user's password to protect their login information
	 */
	private String passHash;

	/*
	 * The socket of the client user we are receiving and sending data to
	 */
	private Socket socket;

	/*
	 * Used to read user input such as login credentials, chat and commands
	 */
	public BufferedReader input;

	/*
	 * Used to send text to the end user printWriter will send the user on this
	 * thread all strings for the program
	 */
	public PrintWriter output;

	/*
	 * Stores a boolean value in main memory that when false will end the thread
	 */
	private volatile boolean connected;

	/*
	 * Stores a boolean value in main memory that tells whether a user is
	 * currently logged in to the chat server
	 */
	private volatile boolean inServer;

	public SessionHandler(Socket socket) {
		this.socket = socket;

	}

	public void run() {
		try {

			/*
			 * Initializing the input and output variables to the input and
			 * output streams of the client's socket respectively
			 */
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new PrintWriter(socket.getOutputStream(), true);

			/*
			 * Set the boolean variable connected to true. This variable is used
			 * to determine whether or not the thread should continue to run.
			 */
			this.connected = true;
			while (connected) {
				if (Server.debug) {
					System.out.println("Connected!");
				}
				String response = "";
				output.println("MENU>>>>>Would you like to Login or Register?");
				if (Server.debug) {
					// output.println("MENU>>>>>Would you like to Login or
					// Register DEBUG TRUE?");
				}
				try {
					/*
					 * Create a file writer and file buffer used to record chat
					 * logs of user and server interaction to the server
					 */

					response = input.readLine().toLowerCase();
					if (Server.debug) {
						System.out.println("User input: " + response);
					}

					/*
					 * In the case that the answer from the client is "register"
					 * then call a method which allows the user to create
					 */
					if (response.equals("register")) {
						new LoginManager().registerLine(input, output, this);
					} else if (response.equals("login")) {
						userName = new LoginManager().loginLine(input, output, this);
						if (userName != null) {
							inServer = true;

							serverconnection: while (inServer) {
								if (!inServer) {
									break serverconnection;
								}
								SessionHandler sh = this;
								Thread inThread = new Thread(new Runnable() {

									@Override
									public void run() {
										while (inServer) {
											if (input != null) {
												try {
													String response = input.readLine();
													if (response.equals("\n") || response.equals("")) {

													} else {
														if (Server.debug) {
															System.out.println(response);
														}
														if (inServer) {
															FileWriter fw = new FileWriter(
																	Server.chatFilePath + "chat.txt", true);
															BufferedWriter bw = new BufferedWriter(fw);
															DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
															if (response.startsWith("/")) {
																String userMessage = userName + ">"
																		+ df.format(new Date()) + ">" + "COMMAND" + ">"
																		+ ">" + ">" + response;
																bw.write(userMessage);
																bw.newLine();
																Command cmnd = new Command(response);
																cmnd.execute(sh);
															} else {
																try {
																	String userMessage = userName + ">"
																			+ df.format(new Date()) + ">" + ">" + ">"
																			+ ">" + response;
																	System.out.println(userMessage);
																	for (SessionHandler session : Server.userSessions) {
																		/*
																		 * Checks
																		 * to
																		 * see
																		 * if
																		 * the
																		 * user
																		 * is
																		 * actually
																		 * in
																		 * the
																		 * server
																		 * before
																		 * sending
																		 * the
																		 * message
																		 * to
																		 * them.
																		 */
																		if (session.inServer) {
																			sendChat(session, userMessage);
																		}
																	}
																	bw.write(userMessage);
																	bw.newLine();
																	System.out.println(userName);
																} finally {
																	if (bw != null) {
																		bw.close();
																	}
																	if (fw != null) {
																		fw.close();
																	}
																}
															}
														}
													}

												} catch (IOException e) {
													if (Server.debug) {
														e.printStackTrace();
													}
													System.out.println("Connection Reset!");
													return;
												}
											}
										}

									}
								});
								inThread.start();
								while (inServer) {
								}
								if (Server.debug) {
									System.out.println("BREAKREACH");
								}
								break serverconnection;

							}
						}
					}
				} catch (SocketException e) {
					/*
					 * If connection to the client is lost inform the server
					 * console and change the connected variable in order to
					 * exit the while loop and end the thread.
					 */
					if (Server.debug) {
						e.printStackTrace();
					}
					System.out.println("We lost one!");
					this.connected = false;
					interrupt();
					return;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setConnected(boolean status) {
		this.connected = status;
	}

	private void sendChat(SessionHandler session, String userMessage) {
		String[] display = userMessage.split(">", 6);
		String[] date = display[1].split(" ");
		session.output.println("ROOM_MESSAGE>>>>>" + "[" + date[1] + "]" + "(" + display[0] + ") " + display[5]);
	}

	public void logout() {
		userName = null;
		inServer = false;
	}

	public Socket getSocket() {
		return socket;
	}

}
