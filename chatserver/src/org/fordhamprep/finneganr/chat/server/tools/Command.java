//MIT License
//
//Copyright (c) 2017 Rory Finnegan
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

package org.fordhamprep.finneganr.chat.server.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;

import org.fordhamprep.finneganr.chat.server.Server;
import org.fordhamprep.finneganr.chat.server.SessionHandler;
import org.fordhamprep.finneganr.chat.server.managers.DatabaseManager;

public class Command {
	String[] command;

	public Command(String input) {
		input = input.split("/", 2)[1];
		command = input.split(" ");

	}

	public void execute(SessionHandler session) {
		if (command[0].equalsIgnoreCase("log")) {
			if (DatabaseManager.checkPermission(session.userName, "commands_admin_log")) {
				try {
					FileReader fr = new FileReader(Server.chatFilePath + "chat.txt");
					BufferedReader br = new BufferedReader(fr);

					String log;
					while ((log = br.readLine()) != null) {
						session.output.println(">>>>>" + log);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				session.output.println(">>>>>You do not have the Permissions needed to use this command!");
			}
		}

		if (command[0].equalsIgnoreCase("logout")) {
			if (DatabaseManager.checkPermission(session.userName, "commands_user_logout")) {
				session.logout();
			} else {
				session.output.println(">>>>>You do not have the Permissions needed to use this command!");
			}
		}

		if (command[0].equalsIgnoreCase("change")) {
			if (command.length < 2) {
				session.output.println(">>>>>Improper Use!");
				session.output.println(">>>>>Proper usage: /change name/pass [username]");
				return;
			}
			if (command.length == 3
					&& (command[1].equalsIgnoreCase("name") || command[1].equalsIgnoreCase("username"))) {
				if (DatabaseManager.checkPermission(session.userName, "commands_user_change_username")) {
					String oldName = session.userName;
					DatabaseManager.changeUsername(session.userName, command[2]);
					session.output.println(">>>>>Username '" + oldName + "' has been changed to '" + command[2] + "'");
					session.userName = command[2];
				} else {
					session.output.println(">>>>>You do not have the Permissions needed to use this command");
				}
			}
			if (command.length == 2
					&& (command[1].equalsIgnoreCase("pass") || command[1].equalsIgnoreCase("password"))) {
				if (DatabaseManager.checkPermission(session.userName, "commands_user_change_password"))
					try {
						session.output.println("CHAT_PASS>>>>>Enter a new Password!");
						String password = session.input.readLine();

						DatabaseManager.changePassword(session.userName, password);
						session.output.println(">>>>>Password Changed!");
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}
		if (command.length == 2 && command[0].equalsIgnoreCase("kick")) {
			if (DatabaseManager.checkPermission(session.userName, "commands_admin_kick")) {
				for (SessionHandler user : Server.userSessions) {
					if (user.userName.equalsIgnoreCase(command[1])) {
						user.logout();
						user.output.println("BOOT>>>>>");
					}
				}
			} else {
				session.output.println(">>>>>You do not have the Permissions needed to use this command");
				
			}
		}
	}

}
