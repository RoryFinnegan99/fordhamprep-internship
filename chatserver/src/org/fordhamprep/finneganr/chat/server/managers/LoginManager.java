//MIT License
//
//Copyright (c) 2017 Rory Finnegan
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

package org.fordhamprep.finneganr.chat.server.managers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketException;

import org.fordhamprep.finneganr.chat.server.Server;
import org.fordhamprep.finneganr.chat.server.SessionHandler;
import org.mindrot.jbcrypt.BCrypt;

public class LoginManager {
	public void registerLine(BufferedReader input, PrintWriter output, SessionHandler session) {
		String username = null;
		String passwordHash = null;
		while (username == null) {
			output.println("MENU_REGISTER>>>>>Enter a valid username:");

			try {
				username = input.readLine();
			} catch (IOException e) {
				session.setConnected(false);
			}

		}

		while (passwordHash == null) {
			output.println("MENU>>>>>Enter a password:");
			try {
				passwordHash = input.readLine();
				/*
				 * Check to see what the user's name and password hash are if
				 * the debug option is set to true
				 */
				if (Server.debug) {
					System.out.println("A user with the following info has just registered:\n" + "username: " + username
							+ "\n" + "Password Hash: " + passwordHash);
				}
			} catch (IOException e) {
				session.setConnected(false);
			}
		}

		/*
		 * If a user with the given username already exists the account will not
		 * be created and the user will be notified.
		 */
		if (!DatabaseManager.createUser(username, passwordHash, "user")) {
			session.output.println(">>>>>A user with this name already exists!");
		}

	}

	public String loginLine(BufferedReader input, PrintWriter output, SessionHandler session) {
		String username = null;
		String password = null;
		while (username == null) {
			output.println("MENU_LOGIN>>>>>Enter a valid username:");
			try {
				username = input.readLine();
			} catch (IOException e) {
				session.setConnected(false);
			}

		}

		while (password == null) {
			output.println("MENU_LOGIN>>>>>Enter a password:");
			try {
				password = input.readLine();

			} catch (IOException e) {
				session.setConnected(false);
			}
		}

		for (SessionHandler sh : Server.userSessions) {
			if (sh.userName != null) {
				if (sh.userName.equalsIgnoreCase(username)) {
					System.out.println(sh.userName);
					sh.logout();
				}
			}
		}

		if (DatabaseManager.authenticateUser(username, password)) {
			output.println("CHAT>>>>>Welcome to the Chat Room!");
			return username;
		} else {
			output.println("MENU>>>>>Incorrect username or password!");
			return null;
		}
	}

}
