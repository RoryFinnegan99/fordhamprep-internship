//MIT License
//
//Copyright (c) 2017 Rory Finnegan
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

package org.fordhamprep.finneganr.chat.server.managers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Properties;

import org.fordhamprep.finneganr.chat.server.Server;

public class ConfigManager {

	public static boolean SQLiteEnabled;

	public static String SQLitePath;

	public static boolean MySQLEnabled;

	public static String MySQLHost;

	public static int MySQLPort;

	public static String MySQLDatabase;

	public static String MySQLUser;

	public static String MySQLPass;

	public static boolean PostgresEnabled;

	public static String PostgresHost;

	public static int PostgresPort;

	public static String PostgresDatabase;

	public static String PostgresUser;

	public static String PostgresPass;

	static InputStream input;

	public static boolean createConfig() {
		File dir = null;
		dir = new File("config.properties");
		if (!dir.exists()) {
			try {
				dir.createNewFile();
				try {
					FileWriter fw = new FileWriter(dir);
					BufferedWriter bw = new BufferedWriter(fw);
					Properties props = new Properties();
					
					bw.write("#CONFIG FILE");
					bw.newLine();
					bw.write("port=3334");
					bw.newLine();
					bw.write("chatPath=chat/");
					bw.newLine();
					bw.write("###################################");
					bw.newLine();
					bw.write("#                                 #");
					bw.newLine();
					bw.write("#            PostgreSQL           #");
					bw.newLine();
					bw.write("#                                 #");
					bw.newLine();
					bw.write("###################################");
					bw.newLine();
					bw.write("enablePostgres=true");
					bw.newLine();
					bw.write("PostgresHost=localhost");
					bw.newLine();
					bw.write("PostgresPort=5432");
					bw.newLine();
					bw.write("PostgresUser=postgres");
					bw.newLine();
					bw.write("PostgresPass=");
					bw.newLine();
					bw.write("PostgresDatabase=chat");
					bw.newLine();
					bw.write("###################################");
					bw.newLine();
					bw.write("#                                 #");
					bw.newLine();
					bw.write("#             MySQL               #");
					bw.newLine();
					bw.write("#                                 #");
					bw.newLine();
					bw.write("###################################");
					bw.newLine();
					bw.write("enableMySQL=false");
					bw.newLine();
					bw.write("MySQLhost=localhost");
					bw.newLine();
					bw.write("MySQLport=3306");
					bw.newLine();
					bw.write("MySQLuser=chatserver");
					bw.newLine();
					bw.write("MySQLpass=blurt");
					bw.newLine();
					bw.write("MySQLdatabase=chatserver");
					bw.newLine();
					bw.write("###################################");
					bw.newLine();
					bw.write("#                                 #");
					bw.newLine();
					bw.write("#             SQLite              #");
					bw.newLine();
					bw.write("#                                 #");
					bw.newLine();
					bw.write("###################################");
					bw.newLine();
					bw.write("enableSQLite=true");
					bw.newLine();
					bw.write("SQLitePath=database/");
					
					bw.close();
					fw.close();
					return false;
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return true;

	}

	public static String getProperty(String fileName, String property) throws IOException {
		Properties props = new Properties();

		input = new FileInputStream(fileName);

		if (input != null) {
			props.load(input);
		} else {
			throw new FileNotFoundException("File " + fileName + " does not exist");
		}

		String value = props.getProperty(property);

		input.close();

		return value;

	}

	public static void grabConfig() {
		try {
			SQLiteEnabled = Boolean.parseBoolean(getProperty("config.properties", "enableSQLite"));
			SQLitePath = getProperty("config.properties", "SQLitePath");
			MySQLEnabled = Boolean.parseBoolean(getProperty("config.properties", "enableMySQL"));
			MySQLHost = getProperty("config.properties", "MySQLhost");
			MySQLPort = Integer.parseInt(getProperty("config.properties", "MySQLport"));
			MySQLDatabase = getProperty("config.properties", "MySQLdatabase");
			MySQLUser = getProperty("config.properties", "MySQLuser");
			MySQLPass = getProperty("config.properties", "MySQLpass");
			PostgresEnabled = Boolean.parseBoolean(getProperty("config.properties", "enablePostgres"));
			PostgresHost = getProperty("config.properties", "PostgresHost");
			PostgresPort = Integer.parseInt(getProperty("config.properties", "PostgresPort"));
			PostgresDatabase = getProperty("config.properties", "PostgresDatabase");
			PostgresUser = getProperty("config.properties", "PostgresUser");
			PostgresPass = getProperty("config.properties", "PostgresPass");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
