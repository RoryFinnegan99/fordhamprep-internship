//MIT License
//
//Copyright (c) 2017 Rory Finnegan
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

package org.fordhamprep.finneganr.chat.server.managers;

import java.io.File;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.fordhamprep.finneganr.chat.server.Server;
import org.mindrot.jbcrypt.BCrypt;

public class DatabaseManager {

	static String[] permissions = { "group_priority", "chat_user_talk", "commands_user_logout", "commands_admin_log",
			"commands_admin_kick", "commands_user_change_username", "commands_user_change_password" };

	/*
	 * In order to modify the database in anyway a connection to the server is
	 * necessary. It is essential that the administrator of the server set ONLY
	 * ONE of the different database interfaces to true as to avoid any fatal
	 * server errors.
	 */
	public static Connection getConnection() {
		if (ConfigManager.SQLiteEnabled) {
			File dir = null;
			dir = new File(ConfigManager.SQLitePath);
			String url = "jdbc:sqlite:" + dir.getPath() + "/database.db";
			if (!(new File(ConfigManager.SQLitePath + "/database.db").exists())) {
				try {
					dir.mkdirs();
					Connection connection = DriverManager.getConnection(url);
					if (connection != null) {
						DatabaseMetaData meta = connection.getMetaData();
						System.out.println("The driver name is " + meta.getDriverName());
						System.out.println("SQLite database has been created.");
						setupDB();
						connection.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			try {
				return DriverManager.getConnection(url);
			} catch (SQLException e) {

				e.printStackTrace();
			}
		} else if (ConfigManager.MySQLEnabled) {
			try {
				/*
				 * Load the implementation of the driver for the MySQL driver
				 */
				Class.forName("com.mysql.jdbc.Driver");
				return DriverManager.getConnection("jdbc:mysql://" + ConfigManager.MySQLHost + ":"
						+ ConfigManager.MySQLPort + "/" + ConfigManager.MySQLDatabase);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
		} else if (ConfigManager.PostgresEnabled) {
			try {
				/*
				 * Load the implementation of the PostreSQL driver for Java
				 */
				Class.forName("org.postgresql.Driver");
				// @formatter:off
				return DriverManager.getConnection("jdbc:postgresql://" + ConfigManager.PostgresHost + 
													":" + ConfigManager.PostgresPort + "/" + 
													ConfigManager.PostgresDatabase, 
													ConfigManager.PostgresUser, ConfigManager.PostgresPass);
				// @formatter:on
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/*
	 * Create a table where user information will be stored Without this table
	 * there is nowhere to store user information and a error will be thrown
	 */
	public static void setupDB() {
		// @formatter:off
		String sql = "CREATE TABLE IF NOT EXISTS users (\n" + "	id integer PRIMARY KEY,\n"
						+ "	username text NOT NULL,\n"
						+ " usergroup text NOT NULL,\n"
						+ "	password_hash text NOT NULL\n"
						+ ");";
		// @formatter:on
		Connection connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			statement.execute(sql);
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		sql = "CREATE TABLE IF NOT EXISTS groups (\n" + " usergroup text PRIMARY KEY,\n";
		for (int i = 0; i < permissions.length; i++) {
			if (i != permissions.length - 1) {
				sql += " " + permissions[i] + " integer NOT NULL, \n";
			} else {
				sql += " " + permissions[i] + " integer NOT NULL \n";
			}
		}
		sql += ");";

		if (Server.debug) {
			System.out.println(sql);
		}

		connection = null;
		try {
			connection = getConnection();
			Statement statement = connection.createStatement();
			statement.execute(sql);
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		sql = "INSERT INTO groups(usergroup";

		for (int i = 0; i < permissions.length; i++) {

			sql += "," + permissions[i];
		}
		sql += ") VALUES(?";
		for (int i = 0; i < permissions.length; i++) {
			sql += ",?";
		}
		sql += ")";

		if (Server.debug) {
			System.out.println(sql);
		}

		try {
			connection = getConnection();
			PreparedStatement prstmnt = connection.prepareStatement(sql);
			try {
				prstmnt.setString(1, "user");
				for (int i = 2; i < permissions.length + 2; i++) {
					if (permissions[i - 2].equals("chat_user_talk") || permissions[i - 2].equals("commands_user_logout")
							|| permissions[i - 2].equals("commands_user_change_username")
							|| permissions[i - 2].equals("commands_user_change_password")) {
						prstmnt.setInt(i, 1);
					}
					else if (permissions[i - 2].equalsIgnoreCase("group_priority")) {
						prstmnt.setInt(i, 3);
					} else {
						prstmnt.setInt(i, 0);
					}
				}
				prstmnt.executeUpdate();
				prstmnt.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
				connection.close();
				if (Server.debug) {
					System.out.println("HERE IS AN ERROR");
				}
			}
			connection = getConnection();
			prstmnt = connection.prepareStatement(sql);
			try {
				prstmnt.setString(1, "admin");
				for (int i = 2; i < permissions.length + 2; i++) {
					if (permissions[i - 2].equalsIgnoreCase("group_priority")) {
						prstmnt.setInt(i, 999);
					} else {
						prstmnt.setInt(i, 1);
					}
				}
				prstmnt.executeUpdate();
				prstmnt.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
				connection.close();
				if (Server.debug) {
					System.out.println("HERE IS AN ERROR");
				}
			}

			createUser("superadmin", BCrypt.hashpw("admin_password", BCrypt.gensalt()), "admin");

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	}

	/*
	 * Used to create a user The method requires a username and a hash of the
	 * user's password. Depending on where the program receives the user
	 * information from they will be assigned a different user group
	 */
	public static synchronized boolean createUser(String username, String passwordHash, String group) {
		int counter = 1;
		String idsql = "SELECT * FROM users ORDER BY id DESC LIMIT 1";
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(idsql);
			if (rs.next()) {
				counter = rs.getInt("id") + 1;
				rs.close();
				statement.close();
				connection.close();
			}

		} catch (SQLException e) {
			setupDB();
		}
		try {
			Connection connection = getConnection();
			PreparedStatement checkstmnt = connection.prepareStatement("SELECT * FROM users WHERE username = ?");
			checkstmnt.setString(1, username);
			if (checkstmnt.executeQuery().next()) {
				System.out.println("USER ALREADY EXISTS!");
				checkstmnt.close();
				connection.close();
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		String sql = "INSERT INTO users(id,username,password_hash,usergroup) " + "VALUES(?,?,?,?)";

		try {
			Connection connection = getConnection();
			PreparedStatement prstmnt = connection.prepareStatement(sql);
			try {
				prstmnt.setInt(1, counter);
				prstmnt.setString(2, username);
				prstmnt.setString(3, passwordHash);
				prstmnt.setString(4, group);
				prstmnt.executeUpdate();
				prstmnt.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
				connection.close();
				if (Server.debug) {
					System.out.println("HERE IS AN ERROR");
				}
			}
			return true;

		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return false;
	}

	public static synchronized boolean authenticateUser(String username, String password) {
		String correctHash;
		String usersql = "SELECT username, password_hash " + "FROM users WHERE username = ?";

		try {
			Connection connection = getConnection();
			PreparedStatement prstmnt = connection.prepareStatement(usersql);
			prstmnt.setString(1, username);

			ResultSet rs = prstmnt.executeQuery();
			if (rs.next()) {
				correctHash = rs.getString("password_hash");
				if (BCrypt.checkpw(password, correctHash)) {
					rs.close();
					prstmnt.close();
					connection.close();
					return true;
				}
				rs.close();
				prstmnt.close();
				connection.close();
				return false;
			} else {
				rs.close();
				prstmnt.close();
				connection.close();
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;

	}

	public static synchronized void changeUsername(String username, String newUsername) {
		String sql = "UPDATE users SET username = ? WHERE username = ?";
		try {
			Connection connection = getConnection();
			PreparedStatement prstmnt = connection.prepareStatement(sql);
			prstmnt.setString(1, newUsername);
			prstmnt.setString(2, username);

			prstmnt.executeUpdate();
			prstmnt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Method used for changing the password of a given user
	 */
	public static synchronized void changePassword(String username, String newPassword) {
		String sql = "UPDATE users SET password_hash = ? WHERE username = ?";
		try {
			Connection connection = getConnection();
			PreparedStatement prstmnt = connection.prepareStatement(sql);

			String passwordHash = BCrypt.hashpw(newPassword, BCrypt.gensalt());

			prstmnt.setString(1, passwordHash);
			prstmnt.setString(2, username);

			prstmnt.executeUpdate();
			prstmnt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Used to check whether or not a specified user is given permission to
	 * access or utilize a certain functionality of the server
	 */
	public static synchronized boolean checkPermission(String username, String permission) {
		try {
			Connection connection = getConnection();
			PreparedStatement prstmnt = connection
					.prepareStatement("SELECT usergroup " + "FROM users WHERE username = ?");
			prstmnt.setString(1, username);

			String userGroup = prstmnt.executeQuery().getString("usergroup");
			prstmnt.close();
			Connection groupCon = getConnection();
			PreparedStatement groupstmnt = groupCon
					.prepareStatement("SELECT " + permission + " FROM groups WHERE usergroup = ?");
			groupstmnt.setString(1, userGroup);
			if (groupstmnt.executeQuery().getInt(permission) == 1) {
				connection.close();
				groupstmnt.close();
				return true;
			} else
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
