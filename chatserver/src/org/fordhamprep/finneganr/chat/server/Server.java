//MIT License
//
//Copyright (c) 2017 Rory Finnegan
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

package org.fordhamprep.finneganr.chat.server;

import java.io.File;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.fordhamprep.finneganr.chat.server.managers.ConfigManager;
import org.fordhamprep.finneganr.chat.server.managers.DatabaseManager;

public class Server {
	
	/*
	 * README:
	 * Dependencies:
	 * JBcrypt
	 * MySQL JDBC
	 * SQLite JDBC
	 * PostgreSQL JDBC
	 */
	
	
	Socket clientSocket;

	/*
	 * A HashSet is used to store user sessions without a possibility of losing
	 * the user's session to another user or having two of the same user
	 */
	private static HashSet<SessionHandler> users = new HashSet<SessionHandler>();

	/*
	 * This is the port that the server will search for incoming traffic on. The
	 * User client will connect to this port to get data from the server
	 */
	static int serverPort;

	/*
	 * Variable when if enabled will debug to server console user input and
	 * server operation details.
	 */
	public static boolean debug;

	/*
	 * This is the path leading to the file that will be used to store the
	 * current server sessions's chat history.
	 */
	public static String chatFilePath;
	
	public static List<SessionHandler> userSessions = new ArrayList<SessionHandler>();

	/*
	 * Constructor for the Server class requiring
	 */
	Server(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	public static void main(String[] args) {
		int sessionEnded;
		
		if(!ConfigManager.createConfig()){
			System.out.println("CONFIG NOT FOUND: CREATING");
			System.exit(0);
		}

		if (args.length == 1) {
			if (args[0].equals("-debug")) {
				debug = true;
			}
		}
		

		ConfigManager.grabConfig();
		DatabaseManager.getConnection();

		/*
		 * Check the config to see what the port for the server should be
		 */
		try {
			serverPort = Integer.parseInt(ConfigManager.getProperty("config.properties", "port"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (Server.debug) {
				System.out.println(chatFilePath);
			}
			chatFilePath = ConfigManager.getProperty("config.properties", "chatPath");
			File dir = new File(chatFilePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			new File(chatFilePath + "chat.txt").createNewFile();

		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * Create a new ServerSocket
		 */
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(serverPort);
		} catch (BindException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("The server has now begun to listen on port: " + serverPort);

		try {
			while (true) {
				SessionHandler session = new SessionHandler(serverSocket.accept());
				session.start();
				userSessions.add(session);
				if(debug){
				System.out.println("{");
				for(SessionHandler sh: userSessions){
					System.out.println(sh.toString());
				}
				System.out.println("}");
			}}
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			try {
				serverSocket.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

	}

}