//MIT License
//
//Copyright (c) 2017 Rory Finnegan
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

var server;

var net = require('net');
var HOST = 'localhost';
var PORT = 3334;
var client = new net.Socket();

let $ = require('jquery');

var info;
var dataBuff;

var logging;

var white = false;

var chatting = false;
var sendingMessage = false;

client.on('data', function(data) {
    info = data;
    console.log(info.toString('utf8'));
    document.getElementById("debug").innerHTML = info;
    if (info.toString('utf8').startsWith("CHAT>>>>>Welcome to the Chat Room!")) {
        chatting = true;
        swapView('loginpage', 'chatpage');
        document.getElementById('title').innerHTML = "Chat App: Chat";
        client.write('howdy \n');
    }

    if (info.toString('utf8').startsWith("MENU>>>>>Incorrect username or password!")) {

    }
    if (info.toString('utf8').startsWith("MENU_LOGIN>>>>>Enter a valid username:")) {
        client.write((document.getElementById('userbox').value + '\n').toString('utf8'));
    }
    if (info.toString('utf8').startsWith("MENU_LOGIN>>>>>Enter a password:")) {
        client.write((document.getElementById('passbox').value + '\n').toString('utf8'));
    }
    if (info.toString('utf8').startsWith("MENU>>>>>Would you like to Login or Register?")) {
        chatting = false;

        clearList();
        swapView('chatpage', 'loginpage');
        document.getElementById('title').innerHTML = "Chat App: Login";

    }
    if (info.toString('utf8').startsWith("BOOT>")) {

    }
    if (info.toString('utf8').startsWith("ROOM_MESSAGE>")) {
        setTimeout(function() {
            addToList(info.toString('utf8'));
        }, 100);


    }

    dataBuff = false;
});

$(document).ready(function() {

    $("#usertext").keyup(function(event) {
        if (event.keyCode == 13) {
            $("#sendbutton").click();
        }
    });

    $("#connectbutton").click(function() {
        connectServer();
    });

    $("#sendbutton").click(function() {
        console.log("Clikkked");
        sendMessage();
    });


    $("#loginbutton").click(function() {
        clickLogin();
    });
});

function handle(e) {
    if (e.keyCode === 13) {
        e.preventDefault();
    }
}

function clickLogin() {
    if (!chatting) {
        if (!logging) {
            if (document.getElementById('userbox').value.length == 0 || document.getElementById('passbox').value.length == 0) {
                setStatus("../images/redx.png", "Enter a valid Username and Password!");

            } else {
                logging = true;
                client.write('login\n');
                logging = false;
            }
        }
    }
}

function clickRegister() {
    if (!chatting) {
        if (!logging) {
            if (document.getElementById('userbox').value.length == 0 || document.getElementById('passbox').value.length == 0) {
                setStatus("../images/redx.png", "Enter a valid Username and Password!");

            }
            logging = true;
            client.write('register\n');
            logging = false;
        }
    }
}

function connectServer() {
    var address = $('#addressbox').val();
    var arr = [];
    arr = address.split(':');
    HOST = arr[0];
    PORT = arr[1];
    client.connect(PORT, HOST, function() {
        console.log('Connected');
    });
    swapView('addresspage', 'loginpage');
}

function sendMessage() {
    console.log("SENDING");
    if (!sendingMessage) {
        console.log("4REAL");
        sendingMessage = true;
        console.log(document.getElementById('usertext').value.toString('utf8'));
        client.write((document.getElementById('usertext').value + '\n').toString('utf8'));
        document.getElementById('usertext').value = "";
        sendingMessage = false;
    }
}


function clearList() {
    $("#messages").empty();
}

function addToList(message) {

    var ul = document.getElementById("messages");
    var li = document.createElement("li");
    if (white) {
        li.className = "white-message"
        white = false;
    } else if (!white) {
        li.className = "gray-message"
        white = true;
    }
    var dir = document.createElement("dir");
    var p = document.createElement("p");
    p.appendChild(document.createTextNode(message));
    dir.appendChild(p);
    li.appendChild(dir);
    ul.appendChild(li);

    $('#messages').scrollTop($('#messages')[0].scrollHeight);

}

function swapView(orig_id, id) {

    document.getElementById(orig_id).style.display = "none";
    document.getElementById(id).style.display = "inline-block";

}

function setStatus(imageLoc, statusText) {
    document.getElementById('status').style.display = "inline-block";
    document.getElementById('statussymbol').src = imageLoc;
    document.getElementById('statussymbol').style.height = "16px";
    document.getElementById('statussymbol').style.width = "16px";
    document.getElementById('statussymbol').style.display = "inline-block";
    document.getElementById('statustext').innerHTML = statusText;
    document.getElementById('statustext').style.display = "inline-block";
}



const remote = require('electron').remote;

function init() {
    document.getElementById("min-btn").addEventListener("click", function(e) {
        const window = remote.getCurrentWindow();
        window.minimize();
    });

    document.getElementById("max-btn").addEventListener("click", function(e) {
        const window = remote.getCurrentWindow();
        if (!window.isMaximized()) {
            window.maximize();
        } else {
            window.unmaximize();
        }
    });

    document.getElementById("close-btn").addEventListener("click", function(e) {
        const window = remote.getCurrentWindow();
        window.close();
    });

};

document.onreadystatechange = function() {
    if (document.readyState == "complete") {
        init();
    }
};