package org.fordhamprep.finneganr.chat.client.tools;

import java.io.IOException;

import org.fordhamprep.finneganr.chat.client.ChatThread;
import org.fordhamprep.finneganr.chat.client.Client;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class FXController extends Application {

	
	@FXML private TextField userField;
	
	@FXML private PasswordField passField;
	
	@FXML private Label labelStatus;
	
	@FXML private Button buttonLogin;

	@Override
	public void start(Stage stage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/login_FXML.fxml"));
			Scene scene = new Scene(root);
			stage.setTitle("Login");
			stage.setScene(scene);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	protected void handleButtonLoginAction(ActionEvent event) {
		String answer;
		Client.out.println("login");
		try {
			while ((answer = Client.in.readLine()) != null) {
				Client.out.println(userField.getText());
				labelStatus.setText(answer);
				while ((answer = Client.in.readLine()) != null) {
					Client.out.println(passField.getText());
					labelStatus.setText(answer);
					while ((answer = Client.in.readLine()) != null) {
						labelStatus.setText(answer);
						while ((answer = Client.in.readLine()) != null) {
							/*
							 * TODO: APP CRASH HERE
							 */
							if (answer.startsWith("CHAT>")) {
								ChatController cc = new ChatController();
								Stage stage = (Stage) buttonLogin.getScene().getWindow();
								stage.hide();
								cc.guiChat(stage);
							} else {
								labelStatus.setText("INCORRECT USERNAME OR PASSWORD!");
							}
							break;
						}
						break;
					}
					break;
				}

			}
		} catch (IOException e) {
			System.err.println("CONNECTION TO THE SERVER HAS BEEN LOST!");
			System.exit(0);
		}
	}

	public static void c() {
		ChatController.printChat();
	}

}
