package org.fordhamprep.finneganr.chat.client.tools;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.fordhamprep.finneganr.chat.client.ChatThread;
import org.fordhamprep.finneganr.chat.client.Client;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ChatController implements Initializable {
	@FXML
	private static TextArea textBox;
	@FXML
	private Button buttonSend;
	@FXML
	private static TextField fieldChat;

	public void guiChat(Stage stage) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/chat_FXML.fxml"));
		loader.setController(this);
		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ChatThread ct = new ChatThread();
		ct.start();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("Document Module initialized.");
		// This will now be called after the @FXML-annotated fields are
		// initialized.
	}

	@FXML
	protected void handleButtonSendAction(ActionEvent event) {
		Client.out.println(fieldChat.getText());
		fieldChat.setText("");
	}

	static public void printChat() {
		textBox.setText(textBox.getText() + "\n" + fieldChat.getText());
	}

}
