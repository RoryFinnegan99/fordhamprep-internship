package org.fordhamprep.finneganr.chat.client;

import java.io.BufferedReader;
import java.io.Console;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import org.fordhamprep.finneganr.chat.client.tools.ConsoleTools;
import org.fordhamprep.finneganr.chat.client.tools.FXController;
import org.mindrot.jbcrypt.BCrypt;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Client implements Runnable {
	/*
	 * This is socket that will be used to receive data from the server
	 */
	public static Socket clientSocket = null;

	/*
	 * This BufferedReader will be used to read data from the server by
	 * accessing the socket above's input stream
	 */
	public static BufferedReader in = null;

	/*
	 * This stream will be used to send data to the server over the socket's
	 * output stream
	 */
	public static PrintStream out = null;

	/*
	 * The inputField BufferedReader will be used to take user input from the
	 * console/textfield
	 */
	public static BufferedReader inputField = null;

	public static boolean closed = false;
	public static String mainclose = "menu";

	public static void main(String[] args) {
		// Initialize HostName and Port
		String hostName = "";
		int port = 3333;
		boolean cli = true;
		ConsoleTools.clearConsole();

		/*
		 * Gives support for launch flags to control application variables
		 * Flags: -host <hostname> hostname is the ip of the server to connect
		 * to -port <portnumber> portnumber is the port to search for the server
		 * on -nogui launches the program without a GUI (Supported)
		 */

		for (byte i = 0; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-host")) {
				hostName = args[i + 1];
			}
			if (args[i].equalsIgnoreCase("-port")) {
				port = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equalsIgnoreCase("-gui")) {
				cli = false;
			}
		}

		try {

			/*
			 * Initalize the clientSocket with the Host and Port Initialize the
			 * inputField with the Client system's input stream Initalize the
			 * out variable with the client's output stream Initialize the in
			 * variable with the Client's input stream
			 */
			clientSocket = new Socket(hostName, port);
			inputField = new BufferedReader(new InputStreamReader(System.in));
			out = new PrintStream(clientSocket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Host name is unrecognized!");
		} catch (IOException e) {
			System.err.println("Could not create an I/O connection to the specified host!");
		}

		if (!cli) {
			Application.launch(FXController.class, args);
		} else {

			if (clientSocket != null && out != null && in != null) {
				try {
					/* Create a thread to read from the server. */
					new Thread(new Client()).start();
					while (!closed) {
						if (mainclose.equals("menu")) {
							String userInput = inputField.readLine().trim();
							out.println(userInput);
							if (userInput.equalsIgnoreCase("/change pass")) {
								mainclose = "pass_chat";
							}
						} else if (mainclose.equals("register")) {
							char password[] = System.console().readPassword();
							String passHash = String.valueOf(password);
							out.println(BCrypt.hashpw(passHash, BCrypt.gensalt()));
							inputField = new BufferedReader(new InputStreamReader(System.in));
							mainclose = "menu";
						} else if (mainclose.equals("login")) {
							char password[] = System.console().readPassword();
							out.println(password);
							inputField = new BufferedReader(new InputStreamReader(System.in));
							mainclose = "menu";
						} else if (mainclose.equals("pass_chat")) {
							out.println(String.valueOf(System.console().readPassword()));
							inputField = new BufferedReader(new InputStreamReader(System.in));
							mainclose = "chat";
						} else if (mainclose.equals("chat")) {
							String userInput = inputField.readLine().trim();
							for (int i = 0; i < userInput.length(); i++) {
								System.out.print('\b');
							}
							out.println(userInput);
						}
					}

					/*
					 * When they are no longer needed the socket along with it's
					 * streams will be closed.
					 */
					out.close();
					in.close();
					clientSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void run() {

		/*
		 * The variable "answer" is the value of text returned from the server
		 */
		String answer;
		try {
			answer = "";
			while ((answer = in.readLine()) != null) {
				String[] display = answer.split(">", 6);
				// /*
				// * Debugging code
				// */
				// for(String s : display){
				// System.out.println(s);
				// }
				System.out.println(display[5]);
				if (answer.startsWith("MENU_REGISTER>")) {
					mainclose = "register";
				}
				if (answer.startsWith("MENU_LOGIN>")) {
					mainclose = "login";
				}
				if (answer.startsWith("MENU>")) {
					mainclose = "menu";
				}
				if (answer.startsWith("CHAT>")) {
					mainclose = "menu";
				}
				if (answer.startsWith("BOOT>")) {
					out.println("");
					mainclose = "menu";
				}
				if (answer.indexOf("*** Ciao") != -1)
					break;
			}
			closed = true;
		} catch (IOException e) {
			System.err.println("CONNECTION TO THE SERVER HAS BEEN LOST!");
			System.exit(0);
		}

	}

}
